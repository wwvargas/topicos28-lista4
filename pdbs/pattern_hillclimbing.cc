#include "pattern_hillclimbing.h"

#include "canonical_pdbs.h"

#include "../globals.h"

#include "../utils/logging.h"

using namespace std;

namespace planopt_heuristics {
vector<set<int>> compute_causally_relevant_variables(const TNFTask &task) {
    /*
        v is causally relevant for w
      iff
        v is a predecessor of w in the causal graph, or
        v is a successor of w in the causal graph and mentioned in the goal
      Since we are considering TNF tasks where all variables are mentioned in
      the goal, this condition simplifies to

        v is causally relevant for w
      iff
        v and w are neighbors in the causal graph.
      iff
        v and w occur in the same operator (and at least one of them changes the state).
    */

    int num_variables = task.variable_domains.size();
    vector<set<int>> relevant(num_variables);
    for (const TNFOperator &op : task.operators) {
        for (const TNFOperatorEntry &e1 : op.entries) {
            for (const TNFOperatorEntry &e2 : op.entries) {
                if (e1.variable_id != e2.variable_id &&
                        (e1.precondition_value != e1.effect_value ||
                         e2.precondition_value != e2.effect_value)) {
                    relevant[e1.variable_id].insert(e2.variable_id);
                }
            }
        }
    }
    return relevant;
}

bool HillClimber::fits_size_bound(const std::vector<Pattern> &collection) const {
    /*
      Compute the number of abstract states in the given collection without
      explicitly computing the projections. Return true if the total size
      is below size_bound and false otherwise.
    */


    // TODO: add your code for exercise (f) here.

    int abstractStates = 0;
    for (const Pattern &p : collection) {
        int patternSize = 1;
        // Gets the number of combinations of variable's values each pattern can have
        for (int var : p) {
            patternSize = patternSize * task.variable_domains[var];
        }
        abstractStates = abstractStates + patternSize;
    }
    return (abstractStates < size_bound);


}


HillClimber::HillClimber(const TNFTask &task, int size_bound, vector<TNFState> &&samples)
    : task(task),
      size_bound(size_bound),
      samples(move(samples)),
      causally_relevant_variables(compute_causally_relevant_variables(task)){
}


vector<Pattern> HillClimber::compute_initial_collection() {
    /*
      In TNF, every variable occurs in the goal state, so we create the
      collection {{v} | v \in V}.
    */
    vector<Pattern> collection;

    // TODO: add your code for exercise (f) here.

    for(int var = 0; var < (int)task.goal_state.size(); var++){
        Pattern p;
        p.push_back(var);
        collection.insert(collection.end(), p);
    }

    return collection;
}

vector<vector<Pattern>> HillClimber::compute_neighbors(const vector<Pattern> &collection) {
    /*
      for each pattern P in the collection C:
          compute the set of variables that are causally relevant for any V in P
             (Use the precomputed information in causally_relevant_variables.)
          remove all variables from this set that already occur in P
          for each variable V in the resulting set:
              add the collection C' := C u {P u {V}} to neighbors

    */
    vector<vector<Pattern>> neighbors;


    // TODO: add your code for exercise (f) here.


    for (const Pattern &p: collection) {
        vector<int> relevant;
        for (int var: p) {
            for (int causallyRelev: causally_relevant_variables[var]){
                // Checks if var is neither already in the relevant list or in the pattern
                if(find(p.begin(), p.end(), causallyRelev) == p.end() &&
                    find(relevant.begin(), relevant.end(), causallyRelev) == relevant.end()){

                    relevant.push_back(causallyRelev);
                }
            }
        }

        for (int v: relevant){

            Pattern newPattern = p;
            // {P u {V}}
            newPattern.push_back(v);
            vector<Pattern> newCollection = collection;
            // C u {P u {V}}
            newCollection.push_back(newPattern);
            // Check if the new collection is smaller than maximum size allowed
            if (fits_size_bound(newCollection)) {
                neighbors.push_back(newCollection);
            }
        }
    }

    return neighbors;
}

vector<int> HillClimber::compute_sample_heuristics(const vector<Pattern> &collection) {
    CanonicalPatternDatabases cpdbs(task, collection);
    vector<int> values;
    values.reserve(samples.size());
    for (const TNFState &sample : samples) {
        values.push_back(cpdbs.compute_heuristic(sample));
    }
    return values;
}

vector<Pattern> HillClimber::run() {
    vector<Pattern> current_collection = compute_initial_collection();
    vector<int> current_sample_values = compute_sample_heuristics(current_collection);

    /*
      current := an initial candidate
      loop forever:
          next := a neighbor of current with maximal improvement over current
          if improvement(next) = 0:
              return current
          current := next

      To measure improvement, use compute_sample_heuristics to compute
      heuristic values for all sample state. Compare the result to
      current_sample_values and count the number of states that have a higher
      heuristic value. Remember to update current_sample_values when you
      modify current_collection.
    */


    // TODO: add your code for exercise (f) here.
    //compute_neighbors(current_collection);
    int improvement;
    while(true){
        vector<Pattern> next;
        vector<vector<Pattern>> neighbors = compute_neighbors(current_collection);
        improvement = 0;
        for(vector<Pattern> n: neighbors){
            vector<int> next_sample_values = compute_sample_heuristics(n);
            int higherH = 0;
            for(int i = 0; i < (int)next_sample_values.size(); i++){
                if(next_sample_values[i] > current_sample_values[i]){
                    higherH++;
                }
            }
            if(higherH > improvement){
                next = n;
                improvement = higherH;
            }
        }
        if(improvement == 0){
            return current_collection;
        }
        current_collection = next;
        current_sample_values = compute_sample_heuristics(current_collection);

    }


    return current_collection;
}
}
